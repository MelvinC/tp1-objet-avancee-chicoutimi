package ca.uqac.registraire;

import java.io.Serializable;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Commande implements Serializable {

	private static final long serialVersionUID = 1L;
	protected COMMAND_TYPE type;
	
	public enum COMMAND_TYPE{
		COMPILATION, CHARGEMENT, CREATION, LECTURE, ECRITURE, FONCTION
	}
	
	public COMMAND_TYPE getType() {
		return this.type;
	}
}
