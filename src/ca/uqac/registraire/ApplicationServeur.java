package ca.uqac.registraire;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;

public class ApplicationServeur {

	private int port;
	private Hashtable map;
	private ServerSocket sSocket;
	private Socket sock;

	public ApplicationServeur(int port) {
		this.port = port;
		this.map = new Hashtable<String, Object>();
		try {
			this.sSocket = new ServerSocket(this.port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void aVosOrdres() {
		while (true)
			try {
				this.sock = sSocket.accept();
				ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
				Commande commande = (Commande) ois.readObject();
				traiterCommande(commande);
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
	}

	public void traiterCommande(Commande uneCommande) {
		switch (uneCommande.getType()) {
		case CHARGEMENT:
			traiterChargement(((Chargement) uneCommande).getName());
			break;
		case COMPILATION:
			traiterCompilation(((Compilation) uneCommande).getSources());
			break;
		case CREATION:
			Class c = null;
			traiterCreation(((Creation) uneCommande).getName(), ((Creation) uneCommande).getId());
			break;
		case ECRITURE:
			Object o = this.map.get(((Ecriture) uneCommande).getId());
			traiterEcriture(o, ((Ecriture) uneCommande).getName(), (Object) ((Ecriture) uneCommande).getValue());
			break;
		case FONCTION:
			Object ob = this.map.get(((Fonction) uneCommande).getId());
			if (((Fonction) uneCommande).getParams().length == 0)
				traiterAppel(ob, ((Fonction) uneCommande).getName(), null, null);
			else {
				String[] types = new String[((Fonction) uneCommande).getParams().length];
				String[] values = new String[((Fonction) uneCommande).getParams().length];
				int i = 0;
				for (String s : ((Fonction) uneCommande).getParams()) {
					String[] tmp = s.split(":");
					types[i] = tmp[0];
					values[i] = tmp[1];
					i++;
				}
				traiterAppel(ob, ((Fonction) uneCommande).getName(), types, values);
			}

			break;
		case LECTURE:
			Object obj = this.map.get(((Lecture) uneCommande).getId());
			traiterLecture(obj, ((Lecture) uneCommande).getAttribute());
			break;
		default:
			break;
		}
	}

	private void traiterAppel(Object o, String name, String[] types, Object[] values) {
		Object result = null;
		try {
			if (types == null || types.length == 0) {
				Method method = o.getClass().getMethod(name);
				result = method.invoke(o);
			} else {
				Class[] classes = new Class[types.length];
				Object[] values_ = new Object[values.length];
				for (int i = 0; i < values.length; i++) {
//For wrapping a primitive class like float to an Object like Float

					if (((String) values[i]).substring(0, 3).equals("ID(")) {
						classes[i] = Class.forName(types[i]);
						String nameValue = ((String) values[i]).split("[(]")[1].substring(0,
								((String) values[i]).split("[(]")[1].length() - 1);
						values_[i] = this.map.get(nameValue);
					} else {
						switch (types[i]) {
						case "int":
							classes[i] = Class.forName("java.lang.Integer");
							values_[i] = Integer.parseInt((String) values[i]);
							break;
						case "short":
							classes[i] = Class.forName("java.lang.Short");
							values_[i] = Short.parseShort((String) values[i]);
							break;
						case "float":
							classes[i] = Class.forName("java.lang.Float");
							values_[i] = Float.parseFloat((String) values[i]);
							break;
						case "double":
							classes[i] = Class.forName("java.lang.Double");
							values_[i] = Double.parseDouble((String) values[i]);
							break;
						case "long":
							classes[i] = Class.forName("java.lang.Long");
							values_[i] = Long.parseLong((String) values[i]);
							break;
						case "bool":
							classes[i] = Class.forName("java.lang.Boolean");
							if (values[i].toString().contains("[Tt]"))
								values_[i] = true;
							else
								values_[i] = false;
							break;
						case "char":
							classes[i] = Class.forName("java.lang.Character");
							values_[i] = (Character) ((String) values[i]).charAt(0);
							break;
						case "void":
							classes[i] = Class.forName("java.lang.Void");
							values_[i] = null;
							break;
						}
					}
				}
				Method method = o.getClass().getMethod(name, classes);
				result = method.invoke(o, values_);
				if(result == null) result="L'appel a la fonction s'est bien d�roul�.";
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | ClassNotFoundException e) {
			result = "L'appel a la fonction a caus� une erreur.";
			e.printStackTrace();
		}
		confirmClient(result);
	}

	private void traiterLecture(Object o, String attribute) {
		Object result = null;
		try {
			Method method = o.getClass()
					.getMethod("get" + attribute.substring(0, 1).toUpperCase() + attribute.substring(1));
			result = method.invoke(o);
			confirmClient(result);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			confirmClient(false);
		}
	}

	private void traiterEcriture(Object o, String name, Object value) {
		Object result = null;
		try {
			Method method = o.getClass().getMethod("set" + name.substring(0, 1).toUpperCase() + name.substring(1),
					value.getClass());
			method.invoke(o, value);
			confirmClient("L'�criture s'est bien d�roul�e.");
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			confirmClient("Une erreur s'est produite lors de l'�criture.");
		}
	}

	private void traiterCreation(String name, String id) {
		try {
			Class[] params = new Class[1];
			params[0] = String.class;
			Constructor c;
			try {
				c = Class.forName(name).getConstructor(params);
				Object obj = c.newInstance(id);
				this.map.put(id, obj);
				confirmClient(this.map.get(id));
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			confirmClient("Une erreur s'est produite lors de la cr�ation.");
		}
	}

	private void traiterCompilation(String[] sources) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		String str = sources[0];
		for (int i = 1; i < sources.length; i++) {
			str += ", " + sources[i];
		}
		processBuilder.command("bash", "-c", "javac " + str);
		try {
			Process process = processBuilder.start();
			confirmClient("La compilation s'est bien d�roul�e.");
		} catch (IOException e) {
			e.printStackTrace();
			confirmClient("Une erreur s'est produite lors de la compilation.");
		}
	}

	private void traiterChargement(String name) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command("bash", "-c", "java -verbose:class " + name.split("\\.")[name.split("\\.").length - 1]);
		try {
			Process process = processBuilder.start();
			confirmClient("Le chargement s'est bien d�roul�.");
		} catch (IOException e) {
			e.printStackTrace();
			confirmClient("Une erreur s'est produite lors du chargement.");
		}
	}

	private void confirmClient(Object o) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(this.sock.getOutputStream());
			oos.writeObject(o);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		ApplicationServeur serv = new ApplicationServeur(7777);
		serv.aVosOrdres();
	}
}
