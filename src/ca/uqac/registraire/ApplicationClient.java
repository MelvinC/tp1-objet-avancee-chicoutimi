package ca.uqac.registraire;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class ApplicationClient {

	private String hostname;
	private int port;
	private BufferedReader commandesReader;
	private PrintStream sortieWriter;

	public Commande saisisCommande(BufferedReader reader) {
		Commande result = null;
		String line = null;
		try {
			line = reader.readLine();
			if(line == null) return null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		String[] tab = line.split("#");
		switch (tab[0]) {
		case "compilation":
			result = new Compilation(tab[1].split(","), tab[2]);
			break;
		case "chargement":
			result = new Chargement(tab[1]);
			break;
		case "creation":
			result = new Creation(tab[1], tab[2]);
			break;
		case "lecture":
			result = new Lecture(tab[1], tab[2]);
			break;
		case "ecriture":
			result = new Ecriture(tab[1], tab[2], tab[3]);
			break;
		case "fonction":
			if (tab.length < 4)
				result = new Fonction(tab[1], tab[2]);
			else
				result = new Fonction(tab[1], tab[2], tab[3].split(","));
			break;
		}
		return result;
	}

	public void initialise(String fichCommandes, String fichSortie) {
		try {
			this.commandesReader = new BufferedReader(new FileReader(
					"D:\\Documents\\ENSISA\\Chicoutimi\\Objet Avanc�e\\TP1\\tp1-objet-avancee-chicoutimi\\src\\ca\\uqac\\registraire\\commandes.txt"));

			this.sortieWriter = new PrintStream(fichSortie);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Object traiteCommande(Commande uneCommande)
			throws UnknownHostException, IOException, ClassNotFoundException {
		Socket sock = new Socket(hostname, port);
		ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
		oos.writeObject(uneCommande);
		ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
		Object o = ois.readObject();
		sock.close();
		return o;
	}

	public void scenario() throws UnknownHostException, IOException, ClassNotFoundException {
		sortieWriter.println("Debut des traitements:");
		Commande prochaine = saisisCommande(commandesReader);
		while (prochaine != null) {
			sortieWriter.println("\tTraitement de la commande " + prochaine + " ...");
			Object resultat = traiteCommande(prochaine);
			sortieWriter.println("\t\tResultat: " + resultat);
			prochaine = saisisCommande(commandesReader);
		}
		sortieWriter.println("Fin des traitements");
	}

	public static void main(String args[]) throws UnknownHostException, IOException, ClassNotFoundException {
		ApplicationClient app = new ApplicationClient();
		app.initialise("commandes.txt", "sorties.txt");
		app.hostname = "localhost";
		app.port = 7777;
		app.scenario();
	}
}
