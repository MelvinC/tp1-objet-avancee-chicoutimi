package ca.uqac.registraire;

import java.util.List;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Fonction extends Commande{

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String[] parameters;
	
	public Fonction(String id, String name, String[] parameters) {
		this.id = id;
		this.name = name;
		this.parameters = parameters;
		this.type = COMMAND_TYPE.FONCTION;
	}
	
	public Fonction(String id, String name) {
		this.id = id;
		this.name = name;
		this.parameters = new String[0];
		this.type = COMMAND_TYPE.FONCTION;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String[] getParams() {
		return this.parameters;
	}
}
