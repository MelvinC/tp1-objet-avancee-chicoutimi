package ca.uqac.registraire;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Chargement extends Commande{

	private static final long serialVersionUID = 1L;
	private String name;

	public Chargement(String name) {
		this.name = name;
		this.type = COMMAND_TYPE.CHARGEMENT;
	}
	
	public String getName() {
		return this.name;
	}
}
