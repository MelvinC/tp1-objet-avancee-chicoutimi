package ca.uqac.registraire;

import java.util.List;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Compilation extends Commande{

	private static final long serialVersionUID = 1L;
	private String[] sources;
	private String class_;

	public Compilation(String[] sources, String class_) {
		this.sources = sources;
		this.class_ = class_;
		this.type = COMMAND_TYPE.COMPILATION;
	}
	
	public String[] getSources() {
		return this.sources;
	}
	
	public String getClass_() {
		return this.class_;
	}
}
