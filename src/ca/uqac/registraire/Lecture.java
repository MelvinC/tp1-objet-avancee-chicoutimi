package ca.uqac.registraire;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Lecture extends Commande{

	private static final long serialVersionUID = 1L;
	private String id;
	private String attribute;
	
	public Lecture(String id, String attribute) {
		this.id = id;
		this.attribute = attribute;
		this.type = COMMAND_TYPE.LECTURE;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getAttribute() {
		return this.attribute;
	}
}
