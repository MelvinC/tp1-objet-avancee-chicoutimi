package ca.uqac.registraire;

import ca.uqac.registraire.Commande.COMMAND_TYPE;

public class Ecriture extends Commande{

	private static final long serialVersionUID = 1L;
	private String id;
	private String name;
	private String value;
	
	public Ecriture(String id, String name, String value) {
		this.id = id;
		this.name = name;
		this.value = value;
		this.type = COMMAND_TYPE.ECRITURE;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getValue() {
		return this.value;
	}
}
