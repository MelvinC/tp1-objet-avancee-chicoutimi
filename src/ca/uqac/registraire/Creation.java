package ca.uqac.registraire;

public class Creation extends Commande{

	private static final long serialVersionUID = 1L;
	private String name;
	private String id;

	public Creation(String name, String id) {
		this.name = name;
		this.id = id;
		this.type = COMMAND_TYPE.CREATION;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
}
